package Comparator;

import java.util.Comparator;

import Model.Company;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company firstObj, Company secondObj) {
		double firstProfits = firstObj.getProfits();
		double secondProfits = secondObj.getProfits();
		if(firstProfits > secondProfits)
			return 1;
		else if(firstProfits < secondProfits)
			return -1;
		else 
			return 0;
	}

}
