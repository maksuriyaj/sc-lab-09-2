package Comparator;

import java.util.Comparator;

import Model.Company;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company firstObj, Company secondObj) {
		double firstExpenses = firstObj.getExpenses();
		double secondExpenses = secondObj.getExpenses();
		if(firstExpenses > secondExpenses)
			return 1;
		else if(firstExpenses < secondExpenses)
			return -1;
		else 
			return 0;
	}

}
