package Comparator;
import java.util.Comparator;

import Model.Company;


public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company firstObj, Company secondObj) {
		double firstIncome = firstObj.getIncome();
		double secondIncome = secondObj.getIncome();
		if(firstIncome > secondIncome)
			return 1;
		else if(firstIncome < secondIncome)
			return -1;
		else 
			return 0;
	}

}
