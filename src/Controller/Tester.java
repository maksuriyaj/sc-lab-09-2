package Controller;

import java.util.ArrayList;
import java.util.Collections;

import Comparator.EarningComparator;
import Comparator.ExpenseComparator;
import Comparator.ProfitComparator;
import Comparator.TaxComparator;
import Interface.Taxable;
import Model.Company;
import Model.Person;
import Model.Product;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testPerson();
		tester.testProduct();
		tester.testCompany();
		tester.testTaxable();
	}

	public void testPerson() {
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 300000));
		persons.add(new Person("Carl", 175, 200000));
		Collections.sort(persons);

		System.out.println("Sort Persons");
		for(Person person:persons){
			System.out.print(person+"\n");
		}
		System.out.println("-----------------------------------");
	}
	public void testProduct() {
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		products.add(new Product("Notebook", 9000));
		Collections.sort(products);

		System.out.println("Sort Products");
		for(Product product:products){
			System.out.print(product+"\n");
		}
		System.out.println("-----------------------------------");
	}
	public void testCompany() {
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		companies.add(new Company("Google", 3000000, 600000));
		Collections.sort(companies, new EarningComparator());

		System.out.println("Sort Company's earnings");
		for(Company company:companies){
			System.out.print(company+"\n");
		}
		System.out.println("-----------------------------------");
		
		Collections.sort(companies, new ExpenseComparator());

		System.out.println("Sort Company's expenses");
		for(Company company:companies){
			System.out.print(company+"\n");
		}
		System.out.println("-----------------------------------");
		
		Collections.sort(companies, new ProfitComparator());

		System.out.println("Sort Company's profits");
		for(Company company:companies){
			System.out.print(company+"\n");
		}
		System.out.println("-----------------------------------");
	}
	public void testTaxable(){
		ArrayList<Taxable> taxables = new ArrayList<Taxable>();
		taxables.add(new Person("Rick", 185, 100000));
		taxables.add(new Product("TV", 10000));
		taxables.add(new Company("Apple", 1000000, 800000));
		
		Collections.sort(taxables, new TaxComparator());
		
		System.out.println("Sort Taxable");
		for(Taxable taxable:taxables){
			System.out.print(((Object)taxable).toString() + "\ntax:" + taxable.getTax()+"\n");
		}
		System.out.println("-----------------------------------");
	}
	
}
